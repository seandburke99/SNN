#include <SNN/Activations.h>
#include <SNN/Layer.h>
#include <stdio.h>
#include <string.h>

uint8_t layer_forward(const Layer* l, const SNNFTYPE* data, SNNFTYPE* result) {
    size_t offset = l->dims[0];
    unsigned int index;
    for (int i = 0; i < l->dims[1]; i++) {
        result[i] = (SNNFTYPE)l->bias[i];
        for (int j = 0; j < l->dims[0]; j++) {
            index = (i * offset) + j;
            result[i] += data[j] * l->weights[index];
        }
    }
    l->act(result, l->dims[1]);
    return 0;
}
