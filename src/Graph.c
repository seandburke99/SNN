#include <SNN/Graph.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

uint8_t graph_forward(const Graph* model, SNNFTYPE* data, SNNFTYPE* result) {
    for (int i = 0; i < model->numLayers; i++) {
        layer_forward(&model->layers[i], data, result);
        memcpy(data, result,
               model->layers[i].dims[model->layers[i].numDims - 1] * sizeof(SNNFTYPE));
    }
    return 0;
}