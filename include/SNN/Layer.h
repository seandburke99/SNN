#ifndef LAYER_H
#define LAYER_H
#include <SNN/SNN_Config.h>
#include <stddef.h>

// An activation function will need to take in a data array and the size of the array.
typedef void (*ActivationFunction)(SNNFTYPE* data, size_t size);

/**
 * @brief Layer structure containing multiple components that make up a layer.
 *
 * @param name Optional pointer to string of characters to name layer.
 * @param dims Pointer to a list of dimensions for the layer.
 * @param numDims Number of dimensions defined for the structure.
 * @param weights Flat representation of the weights in the same form as the
 * dimensions.
 * @param bias Flat array of bias for each node in the layer.
 * @param act Activation function that is applied to nodes in the graph.
 *
 */
typedef struct Layer {
    const char* name;
    const uint32_t* dims;
    uint8_t numDims;
    const SNNTYPE* weights;
    const SNNTYPE* bias;
    ActivationFunction act;
} Layer;

/**
 * @brief Forward function for a layer structure and compatible data.
 *
 * This function will return without performing operations if the pointer
 * does not point to anything.
 *
 * @param l Pointer to initialized layer structure.
 * @param data Pointer to data.
 * @param result Pointer to new data array. Must be preallocated to appropriate size.
 * @return uint8_t 0 on success, 1 on failure.
 */
uint8_t layer_forward(const Layer* l, const SNNFTYPE* data, SNNFTYPE* result);

#endif