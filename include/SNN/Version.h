#ifndef SNN_VERSION_H
#define SNN_VERSION_H
#include <stdint.h>

const extern uint8_t snn_version_major, snn_version_minor, snn_version_patch;

#endif