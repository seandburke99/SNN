#ifndef SNN_CONFIG_H
#define SNN_CONFIG_H
#include <stddef.h>
#include <stdint.h>

// These determine the output type. Can be exchanged for floats but speed will be
// reduced.
#define SNNTYPE int8_t
#define SNNFTYPE int32_t

#endif