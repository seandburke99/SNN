#ifndef ACTIVATIONS_H
#define ACTIVATIONS_H
#include <math.h>
#include <SNN/SNN_Config.h>
#include <stdlib.h>

/**
 * @brief Implements the (Re)ctified (L)inear (U)nit function on input data.
 *
 * @param data Items to pass through the function.
 * @param size Number of items to pass through the function.
 */
void act_relu(SNNFTYPE* data, size_t size);

/**
 * @brief Implements a modified Sigmoid function on input data that scales the
 * value from 0 to 32767.
 *
 * Function: sigmoid = 1/(1+exp(-data))
 *
 * @param data Items to pass through the function.
 * @param size Number of items to pass through the function.
 */
void act_sigmoid(SNNFTYPE* data, size_t size);

/**
 * @brief Implements a modified TanH function on input data that scales the
 * value to +/- 32767
 *
 * @param data Items to pass through the function.
 * @param size Number of items to pass through the function.
 */
void act_tanh(SNNFTYPE* data, size_t size);

/**
 * @brief Lookup table for the sigmoid function. Faster but less precise than
 * the actual function.
 *
 * @param value Value of the independent variable in the function.
 * @return int32_t Value of the dependent variable.
 */
int32_t sigmoid_lut_lookup(int32_t value);

/**
 * @brief Lookup table for the tanh function. Faster but less precise than the
 * actual function.
 *
 * @param value Value of the independent variable.
 * @return int32_t Value of the dependent variable.
 */
int32_t tanh_lut_lookup(int32_t value);

#endif