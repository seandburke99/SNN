#ifndef GRAPH_H
#define GRAPH_H
#include <SNN/Layer.h>
#include <stddef.h>
#include <string.h>

typedef enum {
    GRAPH_FLAGS_NONE = 0,
    GRAPH_FLAGS_STATIC_DEF = 0b1
} GraphFlags;

// Structure to contain the full network
typedef struct {
    const char* name;       // Name of the netural network
    const Layer* layers;    // Array of layers for the network
    const size_t numLayers; // Counter for the number of layers in a network
    uint8_t flags;          // The flags from lsb to msb can be found in the docs
} Graph;

/**
 * @brief Feed forward neural network function of data array through graph.
 *
 * @param model Pointer to graph model to feed through.
 * @param data Pointer to initial data array.
 * @param result Empty pointer that will be assigned final result.
 * @return uint8_t 0 on success, 1 on failure.
 */
uint8_t graph_forward(const Graph* model, SNNFTYPE* data, SNNFTYPE* result);

/**
 * @brief Print out a graph with layer names if defined.
 *
 * @param model Model to print out.
 */
void graph_summarize(const Graph* model);

#endif