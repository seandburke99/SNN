# Seans Neural Networks
Source project for implementing basic neural networks with bare or limited C.
Buildable on linux or mac.

## Build Instructions
Project uses Makefiles, CMake, and Ninja to build. Make sure you have these installed.
Clone the repository and desired branch
```bash
git clone https://github.com/seandburke99/SNN.git -b <branch_name>
```
Move into base directory and invoke make command
```bash
make
```
This will build a static lib. To build for a specific file, just call make and have main.c in the source dir

## Network Setup
The networks in the project are meant to be very statically defined such as the ones in `test/sample_graphs.c`.

## Tests
Tests are added in the cmake and can be invoked via `make test`.
The tests are setup to count the number of failures and then main will result in the total number of failures allowing at least 1 files worth of tests to run before a failure is reported rather than using assert and causing a test to cut short.

## License
MIT License