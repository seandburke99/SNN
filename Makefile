BUILD_DIR = build

.PHONY: debug
debug:
	cmake -S . -B ${BUILD_DIR} -GNinja -DCMAKE_BUILD_TYPE=Debug
	cmake --build ${BUILD_DIR} -j

.PHONY: release
release:
	cmake -S . -B ${BUILD_DIR} -GNinja -DCMAKE_BUILD_TYPE=Release
	cmake --build ${BUILD_DIR} -j

.PHONY: test
test:
	ctest --test-dir build --output-on-failure -j

.PHONY: format
format:
	find . -iname "*.h" -o -iname "*.c" | xargs clang-format -i

.PHONY: lint
lint:
	find . -iname "*.h" -o -iname "*.c" | xargs clang-format -i --dry-run -Werror

.PHONY: clean
clean:
	rm -rf ${BUILD_DIR} src/Version.c