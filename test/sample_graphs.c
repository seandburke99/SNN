#include "sample_graphs.h"

#define WEIGHTS_SMALL_2X4                                                              \
    { 1, 1, 1, 0, 1, 0, 1, 0 }
#define WEIGHTS_SMALL_4X4                                                              \
    { 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 }
#define WEIGHTS_SMALL_4X1                                                              \
    { 1, 1, 0, 0 }

const SNNTYPE weights_small_2x4[] = WEIGHTS_SMALL_2X4;
const SNNTYPE weights_small_4x4[] = WEIGHTS_SMALL_4X4;
const SNNTYPE weights_small_4x1[] = WEIGHTS_SMALL_4X1;

#define BIAS_SMALL_2X4                                                                 \
    { 0, 0, 0, 0 }
#define BIAS_SMALL_4X4                                                                 \
    { 0, 0, 0, 0 }
#define BIAS_SMALL_4X1                                                                 \
    { 0 }

const SNNTYPE bias_small_2x4[] = BIAS_SMALL_2X4;
const SNNTYPE bias_small_4x4[] = BIAS_SMALL_4X4;
const SNNTYPE bias_small_4x1[] = BIAS_SMALL_4X1;

#define TWOBYFOUR                                                                      \
    { 2, 4 }
#define FOURBYFOUR                                                                     \
    { 4, 4 }
#define FOURBYONE                                                                      \
    { 4, 1 }

const uint32_t dims_2x4[] = TWOBYFOUR;
const uint32_t dims_4x4[] = FOURBYFOUR;
const uint32_t dims_4x1[] = FOURBYONE;

const Layer sampleLayers[] = {
    {.name = "Sample Layer Small 2x4",
     .act = act_relu,
     .weights = weights_small_2x4,
     .bias = bias_small_2x4,
     .dims = dims_2x4,
     .numDims = 2},
    {.name = "Sample Layer Small 4x4",
     .act = act_relu,
     .weights = weights_small_4x4,
     .bias = bias_small_4x4,
     .dims = dims_4x4,
     .numDims = 2},
    {.name = "Sample Layer Small 4x1",
     .act = act_relu,
     .weights = weights_small_4x1,
     .bias = bias_small_4x4,
     .dims = dims_4x1,
     .numDims = 2},
};

const Graph sampleGraphs[] = {
    {.name = "Small Sample Graph",
     .numLayers = 3,
     .layers = {&sampleLayers[SAMPLELAYERS_SMALL_2X4],
     &sampleLayers[SAMPLELAYERS_SMALL_4X4],
     &sampleLayers[SAMPLELAYERS_SMALL_4X1]}}
};