#ifndef TEST_TOOLS
#define TEST_TOOLS
#include <stdint.h>
#include <stdio.h>

static inline void log_test_name(const char* name) {
    fprintf(stderr, "-----%s-----\n", name);
}

static inline uint32_t log_error(const char* errstring) {
    fprintf(stderr, "%s,%d: ", __BASE_FILE__, __LINE__);
    fprintf(stderr, "%s", errstring);
    fprintf(stderr, "\n");
    return 1;
}

#endif