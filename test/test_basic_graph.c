#include <sample_graphs.h>
#include <test_tools.h>

uint32_t test_graph_forward_correct(void);

int main(void) {
    uint32_t errors = 0;
    errors += test_graph_forward_correct();
    return errors;
}

uint32_t test_graph_forward_correct(void) {
    log_test_name("test_graph_forward");
    uint32_t errors = 0;
    const Graph* const graph = &sampleGraphs[SAMPLEGRAPHS_SMALL];
    // In and out both need to be large enough to contain the max size the graph gets.
    SNNFTYPE in[4], out[4];
    in[0] = 2;
    in[1] = 1;
    if (graph_forward(graph, in, out)) {
        errors += log_error("Graph forward failed.");
    }
    // The output of the graph is only 1 value (see graph shape).
    if (out[0] != 8) {
        errors += log_error("Graph forward did not result in a 1.");
    }
    return errors;
}