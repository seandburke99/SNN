#ifndef SAMPLE_GRAPH_H
#define SAMPLE_GRAPH_H
#include <SNN/SNN.h>
#include <SNN/SNN_Config.h>

typedef enum SampleGrpahs {
    SAMPLEGRAPHS_SMALL
} SampleGraphs;

typedef enum SampleLayers {
    SAMPLELAYERS_SMALL_2X4,
    SAMPLELAYERS_SMALL_4X4,
    SAMPLELAYERS_SMALL_4X1
} SampleLayers;

const extern Graph sampleGraphs[];
const extern Layer sampleLayers[];

#endif