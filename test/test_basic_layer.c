#include <sample_graphs.h>
#include <test_tools.h>

/**
 * @brief Proper layer forward usage. Should equate to all 1's.
 *
 * @return uint32_t 0 on success, 1 on failure.
 */
uint32_t test_layer_forward_correct(void);

int main(void) {
    uint32_t errors = 0;
    errors += test_layer_forward_correct();
    return errors;
}

uint32_t test_layer_forward_correct(void) {
    log_test_name("test_layer_forward_correct");
    uint32_t errors = 0;
    const Layer* l = &sampleLayers[SAMPLELAYERS_SMALL_2X4];
    SNNFTYPE in[2], out[4];
    in[0] = 1;
    in[1] = 1;
    if (layer_forward(l, in, out))
        errors += log_error("Layer forward failed");
    for (uint8_t i = 0; i < 4; i++) {
        if (!i && out[i] != 2) {
            errors += log_error("Layer forward result produced incorrect calculation.");
        } else if (i && out[i] != 1) {
            errors += log_error("Layer forward result produced incorrect calculation.");
        }
    }
    return errors;
}